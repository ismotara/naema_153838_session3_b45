<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>A simple calculator by using by HTML,CSS, JAVASCRIPT</title>
</head>
<body>
<form name="calc"  >
    <table border="2" style="text-align: center; padding: 5px; background: lightgrey; " cellpadding="5" align="center">
        <tr>
            <td colspan="3"><input type="text" name="display"></td>
            <td><input type="button" value="&larr;" onClick="calc.display.value=calc.display.value.substr(0,calc.display.value.length-1)"></td>
        </tr>
        <tr>
            <td><input type="button" value="0" onClick="calc.display.value+='0'"></td>
            <td><input type="button" value="1" onClick="calc.display.value+='1'"></td>
            <td><input type="button" value="2" onClick="calc.display.value+='2'"></td>
            <td><input type="button" value="+" onClick="calc.display.value+='+'"></td>
        </tr>
        <tr>
            <td><input type="button" value="3" onClick="calc.display.value+='3'"></td>
            <td><input type="button" value="4" onClick="calc.display.value+='4'"></td>
            <td><input type="button" value="5" onClick="calc.display.value+='5'"></td>
            <td><input type="button" value="-" onClick="calc.display.value+='-'"></td>
        </tr>
        <tr>
            <td><input type="button" value="6" onClick="calc.display.value+='6'"></td>
            <td><input type="button" value="7" onClick="calc.display.value+='7'"></td>
            <td><input type="button" value="8" onClick="calc.display.value+='8'"></td>
            <td><input type="button" value="*" onClick="calc.display.value+='*'"></td>
        <tr>
            <td><input type="button" value="9" onClick="calc.display.value+='9'"></td>
            <td><input type="button" value="C" onClick="calc.display.value=''"></td>
            <td><input type="button" value="=" onClick="calc.display.value=eval(calc.display.value)"></td>
            <td><input type="button" value="/" onClick="calc.display.value+='/'"></td>
        </tr>
    </table>
</form>
</body>
</html>